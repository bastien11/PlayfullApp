﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Xml.Linq;
using System.Xml;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace PlayfullApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Quizz : Page
    {        
        public Quizz()
        {
            this.InitializeComponent();
            SystemNavigationManager.GetForCurrentView().BackRequested += OnBackRequested;
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigated += OnNavigated;
            loadQuizz();
        }

        void loadQuizz()
        {
            XDocument XMLdata = XDocument.Load("quizz.xml");
            var dataList = from query in XMLdata.Descendants("question")
            select new Question
            {
                Q = query.Element("q").Value,
                R1 = query.Element("r1").Value,
                R2 = query.Element("r2").Value,
            };
            TheListBox.ItemsSource = dataList;
        }

        public async void Validate_True(Object sender, RoutedEventArgs e)
        {

            ContentDialog dialog = new ContentDialog()
            {
                Title = "Réponse Correct",
                Content = "Bravo !",
                PrimaryButtonText = "OK"
            };
            ContentDialogResult result = await dialog.ShowAsync();
            Frame.Navigate(typeof(Quizz2));

            /* foreach (var child in root.Descendants("question"))
             {                
                 var objQuizz = new Question
                 {
                     R1 = child.Element("r1").Value,
                     R2 = child.Element("r2").Value,
                     R3 = child.Element("r3").Value,
                     R4 = child.Element("r4").Value,
                 };

                 foreach (XElement element in name)
                 {
                     //responses.Add(element.Attribute("id"));



                         ContentDialog dialog = new ContentDialog()
                         {
                             Title = "Erreur de saise",
                             Content = element.Value,
                             PrimaryButtonText = "OK"
                         };
                         ContentDialogResult result = await dialog.ShowAsync();


                 }


             }*/
        }

        public async void Validate_False(Object sender, RoutedEventArgs e)
        {

            ContentDialog dialog = new ContentDialog()
            {
                Title = "Réponse incorrect",
                Content = "Dommage recommence une fois le questionnaire fini!",
                PrimaryButtonText = "OK"
            };
            ContentDialogResult result = await dialog.ShowAsync();
            Frame.Navigate(typeof(Quizz2));
        }

            private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            /*Frame rootFrame = Window.Current.Content as Frame;

            if (rootFrame.CanGoBack)
            {
                e.Handled = true;
                rootFrame.GoBack();
            }*/
            Frame.Navigate(typeof(Quizzs));
        }

        void OnNavigated(Object sender, NavigationEventArgs e)
        {
            if (((Frame)sender).CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }
    }
}
