﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;

using System.Xml.Linq;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PlayfullApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        List<String> logs = new List<String>();
        public MainPage()
        {
            this.InitializeComponent();            
        }

        private async void Connexion_Click(object sender, RoutedEventArgs e)
        {
            XDocument XMLdata = XDocument.Load("user.xml");
            var root = XMLdata.Root;
            foreach (var child in root.Descendants("user"))
            {
                var objUser = new User
                {
                    Login = child.Element("login").Value,
                    Password = child.Element("password").Value
                };
                foreach (XElement element in child.Elements())
                {
                    logs.Add(element.Value);
                }
            }
            if (Login.Text == logs[0] && Password.Password == logs[1])
            {
                Frame.Navigate(typeof(MainAdminPage));                
            }
            else if (Login.Text == logs[2] && Password.Password == logs[3])
            {
                
                Frame.Navigate(typeof(HomePage));
            } else if(Login.Text == logs[4] && Password.Password == logs[5]) {

                Frame.Navigate(typeof(HomePage));
            }

            /*if(logs.Contains(Login.Text) && logs.Contains(Password.Password.ToString()))
            {
                Frame.Navigate(typeof(HomePage));
            }*/

            else
            {
                ContentDialog dialog = new ContentDialog()
                {
                    Title = "Erreur de saise",
                    Content = "Veuillez réessayer !",
                    PrimaryButtonText = "OK"
                };
                ContentDialogResult result = await dialog.ShowAsync();
            }                        
        }
    }
}
