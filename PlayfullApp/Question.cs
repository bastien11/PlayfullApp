﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayfullApp
{
    class Question
    {
        private String q;
        private String r1;
        private String r2;

        public string R1 { get => r1; set => r1 = value; }
        public string R2 { get => r2; set => r2 = value; }
        public string Q { get => q; set => q = value; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
