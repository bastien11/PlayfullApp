﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Xml.Linq;
using Windows.UI.Core;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace PlayfullApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Quizz2 : Page
    {
        public Quizz2()
        {
            this.InitializeComponent();
            SystemNavigationManager.GetForCurrentView().BackRequested += OnBackRequested;
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigated += OnNavigated;
            loadQuizz2();
        }

        void loadQuizz2()
        {
            XDocument XMLdata = XDocument.Load("quizz2.xml");
            var dataList = from query in XMLdata.Descendants("question")
            select new Question
            {
                Q = query.Element("q").Value,
                R1 = query.Element("r1").Value,
                R2 = query.Element("r2").Value,
            };
            TheListBox.ItemsSource = dataList;
        }

        public async void Validate_True(Object sender, RoutedEventArgs e)
        {

            ContentDialog dialog = new ContentDialog()
            {
                Title = "Réponse incorrect",
                Content = "Bravo !",
                PrimaryButtonText = "OK"
            };
            ContentDialogResult result = await dialog.ShowAsync();
            Frame.Navigate(typeof(Quizzs));
        }

        public async void Validate_False(Object sender, RoutedEventArgs e)
        {

            ContentDialog dialog = new ContentDialog()
            {
                Title = "Réponse incorrect",
                Content = "Dommage recommence une fois le questionnaire fini!",
                PrimaryButtonText = "OK"
            };
            ContentDialogResult result = await dialog.ShowAsync();
            Frame.Navigate(typeof(Quizzs));
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            /*Frame rootFrame = Window.Current.Content as Frame;

            if (rootFrame.CanGoBack)
            {
                e.Handled = true;
                rootFrame.GoBack();
            }*/
            Frame.Navigate(typeof(Quizzs));
        }

        void OnNavigated(Object sender, NavigationEventArgs e)
        {
            if (((Frame)sender).CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }
    }
}
